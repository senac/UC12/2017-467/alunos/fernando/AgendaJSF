<%-- 
    Document   : principal
    Created on : 20/08/2018, 20:42:31
    Author     : sala302b
--%>

<%@page import="br.com.senac.agenda.model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <% 

            Usuario usuario = (Usuario) session.getAttribute("user");
            
            if(usuario == null){
                response.sendRedirect("login.html");
            }
        %>
        
        <% 
            out.print(usuario.getNome());
        
        %>
        
        <a href="home.html">Logout</a>
        
    </body>
</html>
