package br.com.senac.academico.model;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

public class Curso {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    
    @Column(length = 200 , nullable = false)
    private String nome;
    
    @ManyToOne(optional = false , fetch = FetchType.EAGER)
    @JoinColumn(name = "Professor_idProfessor" , referencedColumnName = "idProfessor")
    private Professor professor;
    
    @ManyToMany
    @JoinTable(
            name = "CursoAluno" , 
            joinColumns = {
                @JoinColumn(name = "Curso_idCurso" ,referencedColumnName = "idcurso"),
                @JoinColumn(name = "Aluno_idAluno"  , referencedColumnName = "idAluno")
            }
    )
    private List<Aluno> alunos;
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public List<Aluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<Aluno> alunos) {
        this.alunos = alunos;
    }

}
