
package br.com.senac.academico.bean;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;


public class Bean {
    
    public void addMessageInfo(String message){
                    FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage("Sucesso", new FacesMessage(FacesMessage.SEVERITY_INFO, message, "Sucesso"));
    }
    
    public void addMessageErro(String message){
                    FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage("Erro", new FacesMessage(FacesMessage.SEVERITY_ERROR, message, "Sucesso"));
    }
    
    public void addMessageWarnning(String message){
                    FacesContext facesContext = FacesContext.getCurrentInstance();
            facesContext.addMessage("Aviso", new FacesMessage(FacesMessage.SEVERITY_WARN, message, "Aviso"));
    }
    
}
