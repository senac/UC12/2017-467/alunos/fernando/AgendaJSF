
package br.com.senac.academico.bean;

import br.com.senac.academico.dao.ProfessorDAO;
import br.com.senac.academico.model.Professor;
import java.io.Serializable;
import javax.inject.Named;
import javax.faces.view.ViewScoped;


@Named(value = "cadastroProfessor")
@ViewScoped
public class CadastroProfessorBean extends Bean implements Serializable{

        private Professor professor;
        private ProfessorDAO dao;
        
    public CadastroProfessorBean() {
        this.professor = new Professor();
        this.dao = new ProfessorDAO();
    }
    
    public void salvar(){
        
        if (this.professor.getId() == 0){
            dao.save(professor);
            
        }else{
            dao.update(professor);
        }
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }
    
    
    
}
