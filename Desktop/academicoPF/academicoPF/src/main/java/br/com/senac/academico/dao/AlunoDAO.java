
package br.com.senac.academico.dao;

import br.com.senac.academico.model.Aluno;

public class AlunoDAO extends DAO<Aluno>{
    
    public AlunoDAO() {
        super(Aluno.class);
    }
    
    public static void main(String[] args) {
        Aluno aluno = new Aluno();
        aluno.setNome("Fernando");
        aluno.setCidade("Cariacica");
        aluno.setEmail("fernando@hotmail.com");
        aluno.setCpf("9999999999");
        aluno.setEndereco("Rua Jose ");
        aluno.setNumero(100);
        
        
        AlunoDAO dao = new AlunoDAO(); 
        
        dao.save(aluno);
        
    }
    
    
}
