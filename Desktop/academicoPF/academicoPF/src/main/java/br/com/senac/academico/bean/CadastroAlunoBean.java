
package br.com.senac.academico.bean;

import br.com.senac.academico.dao.AlunoDAO;
import br.com.senac.academico.model.Aluno;
import java.io.Serializable;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named(value = "cadastrarAluno")
@ViewScoped
public class CadastroAlunoBean extends Bean implements Serializable{
    
    private Aluno aluno;
    private AlunoDAO dao;
    
    public CadastroAlunoBean() {
        
        this.aluno = new Aluno();
        this.dao = new AlunoDAO();
    }
    
   
    public void salvar(){
        
        if (this.aluno.getId() == 0){
            dao.save(aluno);
            
        }else{
            dao.update(aluno);
        }
    }

    public Aluno getAluno() {
        return aluno;
    }

    public void setAluno(Aluno aluno) {
        this.aluno = aluno;
    }
    
    
    
    
}
