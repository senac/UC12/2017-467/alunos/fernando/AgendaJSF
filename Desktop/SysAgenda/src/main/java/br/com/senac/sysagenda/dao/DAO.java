
package br.com.senac.sysagenda.dao;

import java.util.List;
import javax.persistence.EntityManager;

public abstract class DAO<T> {
    
    protected EntityManager en;
    
    private final Class<T> entidade;
    
    public DAO(Class<T> entidade){
        this.entidade = entidade;
    } 
    
    public void save(T entity){
        this.en = JPAUtil.getEntityManager(); //open connection 
        en.getTransaction().begin(); // start a transaction
        en.persist(entity); // insert
        en.getTransaction().commit(); // finalize a transaction
        en.close(); // close connection
    }
    
    public void update(T entity){
        this.en = JPAUtil.getEntityManager(); //open connection 
        en.getTransaction().begin(); // start a transaction
        en.merge(entity); // overlap
        en.getTransaction().commit(); // finalize a transaction
        en.close(); // close connection
    }
    
    public void delete(T entity){
        this.en = JPAUtil.getEntityManager(); //open connection 
        en.getTransaction().begin(); // start a transaction
        en.remove(en.merge(entity)); // remove
        en.getTransaction().commit(); // finalize a transaction
        en.close(); // close connection
    }
    
    public T find (long id){
        this.en = JPAUtil.getEntityManager(); //open connection 
        en.getTransaction().begin(); // start a transaction
        T t = en.find(entidade, id); // find
        en.getTransaction().commit(); // finalize a transaction
        en.close(); // close connection
        return t;
    }
    
    public List<T> findAll(){
        this.en = JPAUtil.getEntityManager(); //open connection
        List<T> lista; // list
        en.getTransaction().begin(); // start a transaction
        lista = en.createQuery("from " + entidade.getName() + " e ").getResultList(); // look the list
        en.getTransaction().commit(); // finalize a transaction
        en.close(); // close connection
        return lista;
    }
}
