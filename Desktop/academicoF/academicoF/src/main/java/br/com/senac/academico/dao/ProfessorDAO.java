
package br.com.senac.academico.dao;

import br.com.senac.academico.model.Professor;


public class ProfessorDAO extends DAO<Professor>{
    
    public ProfessorDAO(){
        super(Professor.class);
    }
    
}
