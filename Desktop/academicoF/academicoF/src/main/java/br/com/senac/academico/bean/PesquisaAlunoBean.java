package br.com.senac.academico.bean;

import br.com.senac.academico.dao.AlunoDAO;
import br.com.senac.academico.model.Aluno;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "pesquisaAlunoBean")
@ViewScoped
public class PesquisaAlunoBean extends Bean {

    private Aluno alunoSelecionado;
    private AlunoDAO dao;
    private List<Aluno> lista;
    
    private String codigo;
    private String nome;

    public PesquisaAlunoBean() {
        
    }
    
    @PostConstruct
    public void init(){
        try{
            dao = new AlunoDAO();
            alunoSelecionado = new Aluno();
            lista = dao.findAll();
        }catch(Exception ex){
            ex.printStackTrace();
            this.addMessageErro("Falha ao carregar itens.");
        }
    }
    
    public void pesquisa(){
        try {
            this.lista = this.dao.findByFiltro(codigo, nome);

        }catch(Exception ex){
            ex.printStackTrace();
            
        }
    }
    
    public  void salvar(){
        if(this.alunoSelecionado.getId() == 0){
            this.dao.save(alunoSelecionado);
            this.addMessageInfo("Salvo com sucesso");
        }else{
            this.dao.update(alunoSelecionado);
            this.addMessageInfo("Atualizado com sucesso");
        }
    }
    
    public void deletar(Aluno aluno){
        try{
            this.dao.delete(aluno);
            this.addMessageInfo("Deletado com sucesso");
            this.pesquisa();
        }catch(Exception ex){
            ex.printStackTrace();
            this.addMessageErro("Erro ao deletar!");
        }
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    
    
    public Aluno getAlunoSelecionado() {
        return alunoSelecionado;
    }

    public void setAlunoSelecionado(Aluno alunoSelecionado) {
        this.alunoSelecionado = alunoSelecionado;
    }

    public AlunoDAO getDao() {
        return dao;
    }

    public void setDao(AlunoDAO dao) {
        this.dao = dao;
    }

    public List<Aluno> getLista() {
        return lista;
    }

    public void setLista(List<Aluno> lista) {
        this.lista = lista;
    }
    
    

}
