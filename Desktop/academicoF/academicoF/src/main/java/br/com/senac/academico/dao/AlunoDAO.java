
package br.com.senac.academico.dao;

import br.com.senac.academico.model.Aluno;
import java.util.List;
import javax.persistence.Query;

public class AlunoDAO extends DAO<Aluno>{
    
    public AlunoDAO() {
        super(Aluno.class);
    }
    
    public static void main(String[] args) {
       /* Aluno aluno = new Aluno();
        aluno.setNome("Fernando");
        aluno.setCidade("Cariacica");
        aluno.setEmail("fernando@hotmail.com");
        aluno.setCpf("9999999999");
        aluno.setEndereco("Rua Jose ");
        aluno.setNumero(100);
        
        
        AlunoDAO dao = new AlunoDAO(); 
        
        dao.save(aluno);*/
        
    }
    
   public List<Aluno> findByFiltro(String codigo, String nome) {
        this.em = JPAUtil.getEntityManager();
        List<Aluno> lista;
        em.getTransaction().begin();

        StringBuilder sql = new StringBuilder("from Aluno a where 1=1 ");

        if (codigo != null && !codigo.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sql.append(" and a.nome like :Nome ");
        }
        
         Query query = em.createQuery(sql.toString());
        
        if (codigo != null && !codigo.isEmpty()) {
           query.setParameter("Id", new Long(codigo));
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Nome", "%" + nome + "%");
        }


        lista = query.getResultList();

        em.getTransaction().commit();
        em.close();

        return lista;
    }
}
